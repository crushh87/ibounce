package CmdHandlers;

import org.bukkit.command.Command;
import org.bukkit.command.CommandSender;

public class Help {

	public Help(CommandSender s, Command c, String[] a) {
		
		
		if(a.length == 1){
			
			if(s.hasPermission("ibounce.help")){
				
				s.sendMessage("�6--> �3iBounce �6<---");
				s.sendMessage("�6Created by �3crushh87");
				s.sendMessage("�6Commands�3:");
				s.sendMessage("�3/ibounce help �6 - Shows the help page.");
				s.sendMessage("�3/ibounce create <name> <velocity> �6- Create a bounce block.");
				s.sendMessage("�3/ibounce list �6- Lists all bounce blocks." );
				s.sendMessage("�3/ibounce delete <name> �6- Deletes a bounce block.");
				
			}else{
				s.sendMessage("�3[�6iBounce�3] �6No permission.");
			}
		}else{
			s.sendMessage("�3[�6iBounce�3] �6 Incorrect usage (/ibounce help)");
		}
	}

}
