package CmdHandlers;

import java.io.IOException;

import org.bukkit.command.Command;
import org.bukkit.command.CommandSender;

import devJack.co.iBounce.Main;

public class deleteBounceBlock {

	private Main pl;

	public deleteBounceBlock(Main instance) {
	pl = instance;

	}
		
	@SuppressWarnings("static-access")
	public deleteBounceBlock(CommandSender s, Command c, String[] a) {
		
		if(s.hasPermission("ibounce.delete")){
			
			if(a.length == 2){
				
				String name = a[1];
				
				if(pl.getConfig().contains(name)){
					pl.getConfig().set(name, null);
					s.sendMessage(pl.prefix + "Block deleted.");
					pl.saveConfig();
				}else{
					s.sendMessage(pl.prefix + "Name not found.");
				}
				
				
				
				
			}else{
				s.sendMessage(pl.prefix + "Incorrect Usage (/ibounce delete <name>");
			}	
		}else{
			s.sendMessage(pl.prefix + "No permission");
		}
		
		
	}

}
