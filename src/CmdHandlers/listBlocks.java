package CmdHandlers;

import org.bukkit.ChatColor;
import org.bukkit.command.Command;
import org.bukkit.command.CommandSender;

import devJack.co.iBounce.Main;

public class listBlocks {
	private Main pl;

	public listBlocks(Main instance) {
	pl = instance;

	}

	@SuppressWarnings("static-access")
	public listBlocks(CommandSender s, Command c, String[] a) {
		
		if(s.hasPermission("ibounce.list")){
		
		if(a.length == 1){	
			s.sendMessage(pl.prefix + "Current Bounce Blocks:");
		for (String key : pl.getConfig().getKeys(false)){
			s.sendMessage(ChatColor.GOLD + key);
		}
		
		}else{
			s.sendMessage(pl.prefix + "Incorrect Usage (/ibounce list)");
		}
		
		}else{
			s.sendMessage(pl.prefix + "No Permission");
		}
	}

}
