package devJack.co.iBounce;

import net.minecraft.server.v1_6_R2.Material;

import org.bukkit.Bukkit;
import org.bukkit.Location;
import org.bukkit.entity.Entity;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.block.Action;
import org.bukkit.event.entity.EntityDamageEvent;
import org.bukkit.event.player.PlayerInteractEvent;
import org.bukkit.event.player.PlayerMoveEvent;
import org.bukkit.util.Vector;

public class Events implements Listener  {
	private Main pl;

	public Events(Main instance) {
	pl = instance;

	}
	
	
	@SuppressWarnings("static-access")
	@EventHandler
	public void onPlayerInteract(PlayerInteractEvent ev){
	
	if(ev.getAction() == Action.RIGHT_CLICK_BLOCK){
		
			
		if(pl.data.contains(ev.getPlayer().getName())){
			for (String name1 : pl.nameData){
				pl.getConfig().set(name1 + ".x", ev.getClickedBlock().getX());
				pl.getConfig().set(name1 + ".y", ev.getClickedBlock().getY());
				pl.getConfig().set(name1 + ".z", ev.getClickedBlock().getZ());
				pl.getConfig().set(name1 + ".w", ev.getClickedBlock().getWorld().getName());
				
				for(Integer vect : pl.velocityData){
					pl.getConfig().set(name1 + ".velocity", vect);
					break;
				}
				pl.nameData.clear();
				pl.velocityData.clear();
				pl.saveConfig();
				ev.getPlayer().sendMessage(pl.prefix + "Block saved.");
				pl.data.remove(ev.getPlayer().getName());
				break;
			}
			
			
		}
			
		}

		
	}
	
	
	@SuppressWarnings("static-access")
	@EventHandler
	public void onPlayerMove(PlayerMoveEvent ev){
		

		
		for (String keys : pl.getConfig().getKeys(false)){
		
				Location loc = ev.getPlayer().getLocation();
			
				int x = loc.getBlockX();
				int y = loc.getBlockY();
				int z = loc.getBlockZ();
				String w = loc.getWorld().getName();
			
				int x2 = pl.getConfig().getInt(keys + ".x");
				int y2 = pl.getConfig().getInt(keys + ".y");
				int z2 = pl.getConfig().getInt(keys + ".z");
				String w2 = pl.getConfig().getString(keys + ".w");
				
				
			
				
				
				if(x == x2 && y == y2 + 1 && z == z2){
					
					final Entity e = ev.getPlayer();
					double velo = pl.getConfig().getDouble(keys + ".velocity");
					
					
					Vector direction = ev.getPlayer().getEyeLocation().getDirection();	
			        e.setVelocity(direction.multiply(velo));
			        pl.playerData.add(ev.getPlayer().getName());
					
				}
				
				
			
		}
		
		
		
	}
	@SuppressWarnings("static-access")
	@EventHandler
	public void onHurt(EntityDamageEvent ev){
		if(ev.getCause().equals(EntityDamageEvent.DamageCause.FALL)){
			Entity e = ev.getEntity();
			
			if(e instanceof Player){
				if(pl.playerData.contains(((Player) e).getName())){
					ev.setCancelled(true);
					pl.playerData.remove(((Player) e).getName());
					
				}
			}
			
			
			
		}
	}
	
}

